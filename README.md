# Flexible Query Builder for the hybris [y] platform

## Introduction
The **Flexible Query Builder** provides a convenient, fluent interface to creating `FlexibleSearchQuery` objects. It **does not run the query** because there is no dependency on the *FlexibleSearchService*.

## Selects
**Specifying A Select Clause**

```java
Qwry.type("products").select("name", "price").build();
```

The `distinct` method allows you to force the query to return distinct results:
```java
Qwry.type("products").select("price").distinct().build();
```

**Column Fallback**

When no `select` is specified the query builder will select **all columns (SELECT *)** from the table.
```java
Qwry.type("products").build();
// equals => SELECT * FROM {products};
```

## Aggrates
The query builder also provides a variety of aggregate methods, such as `count`, `max`, `min`, `avg`, and `sum`.

```java
Qwry.type("products").count();

Qwry.type("products").max("price");

Qwry.type("products").min("price");

Qwry.type("products").avg("price");

Qwry.type("products").sum("price");
```

Of course, you may combine these methods with other clauses to build your query:
```java
Qwry.type("products").where("price", ">", 500).count();
```

## Joins
// TODO

## Unions
// TODO

## Where Clauses

#### Simple Where Clauses
To add `where` clauses to the query, use the `where` method on a query builder instance. The most basic call to `where` requires three arguments. The first argument is the name of the column. The second argument is an operator, which can be any of the database's supported operators. The third argument is the value to evaluate against the column.

For example, here is a query that verifies the value of the "code" column is equal to 'sku0001':
```java
Qwry.type("products").where("code", "=", "sku0001").build();
```

For convenience, if you simply want to verify that a column is equal to a given value, you may pass the value directly as the second argument to the `where` method:
```java
Qwry.type("products").where("code", "sku0001").build();
```

Of course, you may use a variety of other operators when writing a `where` clause:
```java
Qwry.type("products").where("price", ">=", 100).build();

Qwry.type("products").where("code", "<>", "sku0001").build();

// NOTE: This can also done with the 'whereLike' method
Qwry.type("products").where("code", "like", "sku000%").build();
```

#### Additional Where Clauses

###### whereBetween / whereNotBetween
The `whereBetween` method verifies that a column's value is between two values:
```java
Qwry.type("products").whereBetween("price", [50, 200]).build();
```

The `whereNotBetween` method verifies that a column's value lies outside of two values:
```java
Qwry.type("products").whereNotBetween("price", [50, 200]).build();
```
###### whereIn / whereNotIn
The `whereIn` method verifies that a given column's value is contained within the given array:
```java
Qwry.type("products").whereIn("sku", [50, 200]).build();
```
The `whereNotIn` method verifies that the given column's value is *not* contained in the given array:
```java
Qwry.type("products").whereNotIn("sku", [50, 200]).build();
```

###### whereNull / whereNotNull
The `whereNull` method verifies that the value of the given column is `NULL`:
```java
Qwry.type("products").whereNull("imageContainer").build();
```

The `whereNotNull` method verifies that the column's value is *not* `NULL`:
```java
Qwry.type("products").whereNotNull("imageContainer").build();
```

### Advanced Where Clauses

*Parameter Grouping*

Sometimes you may need to create more advanced where clauses such as nested parameter groupings. The Flexible Query Builder can handle these as well. To get started, let's look at an example of grouping constraints within parenthesis:

```java
Qwry.type("users")
    .where("name", "=", "John")
    .andWhere(query ->
        query.where("votes", ">", 100)
             .andWhere("title", "<>", "admin")
             .orWhere("title", "<>", "admin")
    )
    .orWhere(query ->
        query.where("votes", ">", 100)
             .andWhere("title", "<>", "admin")
             .orWhere("title", "<>", "admin")
    )
    .build();
```

## Ordering
// TODO

## Grouping
// TODO

## Limit & Offset
// TODO
