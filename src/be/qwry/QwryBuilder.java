package be.qwry;

import be.qwry.components.Aggregate;
import be.qwry.components.Column;
import be.qwry.components.WhereClause;
import be.qwry.constants.QwryConstants;
import be.qwry.grammar.GrammerParser;
import com.google.common.collect.Lists;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Query builder for the hybris [y] platform
 *
 * @author Mathias Keustermans
 * @version 0.1
 */
public class QwryBuilder {

    /**
     * The database query grammar instance.
     */
    private GrammerParser _grammerParser = new FlexibleSearchParser();

    /**
     * The table which the query is targeting.
     */
    protected String _tableName;

    /**
     * The alias of the table.
     */
    protected String _tableAlias;

    /**
     * Indicates if the query returns distinct results.
     */
    protected boolean _isDistinct;

    /**
     * Indicates if the query should include subtypes of the queries table.
     */
    protected boolean _includeSubTypes = false;

    /**
     * The columns that should be returned.
     */
    protected List<Object> _columns = new ArrayList<>();

    /**
     * The where constraints for the query.
     */
    protected List<WhereClause> _wheres= new LinkedList<>();

    /**
     * An aggregate function and column to be run.
     */
    protected Aggregate _aggregate;

//    protected String _orderDirection;

    /**
     * Create a new query builder instance.
     *
     * @param tableName             The table which the query is targeting.
     * @since 0.1
     */
    public QwryBuilder(final String tableName) {
        this._tableName = tableName;
    }

    /**
     * Create a new query builder instance.
     *
     * @param tableName             The table which the query is targeting.
     * @param tableAlias            Alias of the table
     * @since 0.1
     */
    public QwryBuilder(final String tableName, final String tableAlias) {
        this._tableName = tableName;
        this._tableAlias = tableAlias;
    }

    /**
     * Indicates if the query should include subtypes of the queries table.
     * @since 0.1
     */
    public QwryBuilder includeSubtypes() {
        this._includeSubTypes = true;
        return this;
    }

    /**
     * Set the columns to be selected.
     *
     * @param columns   The columns that should be returned.
     * @return Instance of the QWRY builder.
     * @since 0.1
     */
    public QwryBuilder select(final Object... columns) {

        if(columns == null) {
            throw new IllegalArgumentException("Select columns can not be null!");
        }

        this._columns = Lists.newArrayList(columns);
        return this;
    }

    /**
     * Force the query to only return distinct results.
     *
     * @return Instance of the QWRY builder.
     * @since 0.1
     */
    public QwryBuilder distinct() {
        this._isDistinct = true;
        return this;
    }

    private QwryBuilder _addWhereClause(final WhereClause whereClause) {
        this._wheres.add(whereClause);
        return this;
    }

    private QwryBuilder addWhereClause(final Column column, final String condition, final Object value, final String type) {

        if(value instanceof Boolean) {
            // This has to be done because not every database can handle true/false.
            if(Boolean.TRUE.equals(value)) {
                this._addWhereClause(new WhereClause(column, condition, 1, type));
            } else {
                this._addWhereClause(new WhereClause(column, condition, 0, type));
            }

            return this;
        }

        this._addWhereClause(new WhereClause(column, condition, value, type));
        return this;
    }

    public QwryBuilder where(final String column, final Object value) {
        return this.addWhereClause(new Column(column), "=", value, "AND");
    }

    public QwryBuilder where(final Column column, final Object value) {
        return this.addWhereClause(column, "=", value, "AND");
    }

    public QwryBuilder where(final String column, final String condition, final Object value) {
        return this.addWhereClause(new Column(column), condition, value, "AND");
    }

    public QwryBuilder where(final Column column, final String condition, final Object value) {
        return this.addWhereClause(column, condition, value, "AND");
    }

    public QwryBuilder andWhere(final String column, final Object value) {
        return this.addWhereClause(new Column(column), "=", value, "AND");
    }

    public QwryBuilder andWhere(final Column column, final Object value) {
        return this.addWhereClause(column, "=", value, "AND");
    }

    public QwryBuilder andWhere(final String column, final String condition, final Object value) {
        return this.addWhereClause(new Column(column), condition, value, "AND");
    }

    public QwryBuilder andWhere(final Column column, final String condition, final Object value) {
        return this.addWhereClause(column, condition, value, "AND");
    }

    public QwryBuilder andWhere(final Consumer<QwryNestedWhereClauseBuilder> query) {
        final QwryNestedWhereClauseBuilder builder = new QwryNestedWhereClauseBuilder();
        query.accept(builder);
        this._addWhereClause(new WhereClause(builder.getWhereClauses(), "AND"));
        return this;
    }

    public QwryBuilder orWhere(final String column, final Object value) {
        return this.addWhereClause(new Column(column), "=", value, "OR");
    }

    public QwryBuilder orWhere(final Column column, final Object value) {
        return this.addWhereClause(column, "=", value, "OR");
    }

    public QwryBuilder orWhere(final String column, final String condition, final Object value) {
        return this.addWhereClause(new Column(column), condition, value, "OR");
    }

    public QwryBuilder orWhere(final Column column, final String condition, final Object value) {
        return this.addWhereClause(column, condition, value, "OR");
    }

    public QwryBuilder orWhere(final Consumer<QwryNestedWhereClauseBuilder> query) {
        final QwryNestedWhereClauseBuilder builder = new QwryNestedWhereClauseBuilder();
        query.accept(builder);
        this._addWhereClause(new WhereClause(builder.getWhereClauses(), "OR"));
        return this;
    }

    // FIXME maybe don't do the "%" ourselfs but have this responsibility with the user
        /*
        hybris docs

        LIKE
Compares to a pattern.
... WHERE ... LIKE '...'

%
Wildcard matching any number of characters.
... WHERE ... LIKE '%...'||'...%...'||'...%'

_
Wildcard matching a single character.
... WHERE ... LIKE '...' || '......' ||'..._'
         */

    public QwryBuilder whereLike(final String column, final String value) {
        return this.where(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryBuilder whereLike(final Column column, final String value) {
        return this.where(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryBuilder andWhereLike(final String column, final String value) {
        return this.andWhere(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryBuilder andWhereLike(final Column column, final String value) {
        return this.andWhere(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryBuilder orWhereLike(final String column, final String value) {
        return this.orWhere(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryBuilder orWhereLike(final Column column, final String value) {
        return this.orWhere(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryBuilder whereNull(final String column) {
        return this.where(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryBuilder whereNull(final Column column) {
        return this.where(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryBuilder andWhereNull(final String column) {
        return this.andWhere(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryBuilder andWhereNull(final Column column) {
        return this.andWhere(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryBuilder orWhereNull(final String column) {
        return this.orWhere(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryBuilder orWhereNull(final Column column) {
        return this.orWhere(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryBuilder whereNotNull(final String column) {
        return this.where(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryBuilder whereNotNull(final Column column) {
        return this.where(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryBuilder andWhereNotNull(final String column) {
        return this.andWhere(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryBuilder andWhereNotNull(final Column column) {
        return this.andWhere(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryBuilder orWhereNotNull(final String column) {
        return this.orWhere(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryBuilder orWhereNotNull(final Column column) {
        return this.orWhere(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

//    public QWRYBuilder orderBy(final String column) {
//        return this.orderBy(column, "ASC");
//    }

//    public QWRYBuilder orderBy(final String column, final String direction) {
//        this._orderDirection = direction;
//        return this;
//    }

    /**
     * ==============================================================
     * Aggregates section
     * ==============================================================
     */

    /**
     * Retrieve the "count" result of the query.
     *
     * @return Instance of a FlexibleSearchQuery
     * @since 0.1
     */
    public FlexibleSearchQuery count() {
        return this.count("*");
    }

    /**
     * Retrieve the "count" result of a given column.
     *
     * @return Instance of a FlexibleSearchQuery
     * @since 0.1
     */
    public FlexibleSearchQuery count(final String column) {
        this._aggregate = new Aggregate("COUNT", column);
        return this.build();
    }

    /**
     * Retrieve the maximum value of a given column.
     *
     * @return Instance of a FlexibleSearchQuery
     * @since 0.1
     */
    public FlexibleSearchQuery max(final String column) {
        this._aggregate = new Aggregate("MAX", column);
        return this.build();
    }

    /**
     * Retrieve the minimum value of a given column.
     *
     * @return Instance of a FlexibleSearchQuery
     * @since 0.1
     */
    public FlexibleSearchQuery min(final String column) {
        this._aggregate = new Aggregate("MIN", column);
        return this.build();
    }

    /**
     * Retrieve the average value of a given column.
     *
     * @return Instance of a FlexibleSearchQuery
     * @since 0.1
     */
    public FlexibleSearchQuery avg(final String column) {
        this._aggregate = new Aggregate("AVG", column);
        return this.build();
    }

    /**
     * Retrieve the sum of the values of a given column.
     *
     * @return Instance of a FlexibleSearchQuery
     * @since 0.1
     */
    public FlexibleSearchQuery sum(final String column) {
        this._aggregate = new Aggregate("SUM", column);
        return this.build();
    }

    /**
     * ==============================================================
     * Joins section
     * ==============================================================
     */
    // TODO public FlexibleSearchQuery join(final String table, final String )



    // TODO add "debug" function
    public FlexibleSearchQuery build() {
        return this._grammerParser.compile(this);
    }

    /**
     * ==============================================================
     * Static Helper Methods
     * ==============================================================
     */
    public static Column col(final String column) {
        return new Column(column);
    }

    public static Column col(final String tableAliasPrefix, final String column) {
        return new Column(tableAliasPrefix, column);
    }
}

