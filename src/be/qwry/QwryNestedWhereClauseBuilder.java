package be.qwry;

import be.qwry.components.Column;
import be.qwry.components.WhereClause;
import be.qwry.constants.QwryConstants;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mathias on 05/04/16.
 */
public class QwryNestedWhereClauseBuilder {

    protected List<WhereClause> _wheres = new LinkedList<>();

    private QwryNestedWhereClauseBuilder _addWhereClause(final WhereClause whereClause) {
        this._wheres.add(whereClause);
        return this;
    }

    private QwryNestedWhereClauseBuilder addWhereClause(final Column column, final String condition, final Object value, final String type) {

        if(value instanceof Boolean) {
            // This has to be done because not every database can handle true/false.
            if(Boolean.TRUE.equals(value)) {
                this._addWhereClause(new WhereClause(column, condition, 1, type));
            } else {
                this._addWhereClause(new WhereClause(column, condition, 0, type));
            }

            return this;
        }

        this._addWhereClause(new WhereClause(column, condition, value, type));
        return this;
    }

    public QwryNestedWhereClauseBuilder where(final String column, final Object value) {
        return this.addWhereClause(new Column(column), "=", value, QwryConstants.WhereType.AND);
    }

    public QwryNestedWhereClauseBuilder where(final Column column, final Object value) {
        return this.addWhereClause(column, "=", value, QwryConstants.WhereType.AND);
    }

    public QwryNestedWhereClauseBuilder where(final String column, final String condition, final Object value) {
        return this.addWhereClause(new Column(column), condition, value, QwryConstants.WhereType.AND);
    }

    public QwryNestedWhereClauseBuilder where(final Column column, final String condition, final Object value) {
        return this.addWhereClause(column, condition, value, QwryConstants.WhereType.AND);
    }

    public QwryNestedWhereClauseBuilder andWhere(final String column, final Object value) {
        return this.addWhereClause(new Column(column), "=", value, QwryConstants.WhereType.AND);
    }

    public QwryNestedWhereClauseBuilder andWhere(final Column column, final Object value) {
        return this.addWhereClause(column, "=", value, QwryConstants.WhereType.AND);
    }

    public QwryNestedWhereClauseBuilder andWhere(final String column, final String condition, final Object value) {
        return this.addWhereClause(new Column(column), condition, value, QwryConstants.WhereType.AND);
    }

    public QwryNestedWhereClauseBuilder andWhere(final Column column, final String condition, final Object value) {
        return this.addWhereClause(column, condition, value, QwryConstants.WhereType.AND);
    }

    public QwryNestedWhereClauseBuilder orWhere(final String column, final Object value) {
        return this.addWhereClause(new Column(column), "=", value, QwryConstants.WhereType.OR);
    }

    public QwryNestedWhereClauseBuilder orWhere(final Column column, final Object value) {
        return this.addWhereClause(column, "=", value, QwryConstants.WhereType.OR);
    }

    public QwryNestedWhereClauseBuilder orWhere(final String column, final String condition, final Object value) {
        return this.addWhereClause(new Column(column), condition, value, QwryConstants.WhereType.OR);
    }

    public QwryNestedWhereClauseBuilder orWhere(final Column column, final String condition, final Object value) {
        return this.addWhereClause(column, condition, value, QwryConstants.WhereType.OR);
    }

    public QwryNestedWhereClauseBuilder whereLike(final String column, final String value) {
        return this.where(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryNestedWhereClauseBuilder whereLike(final Column column, final String value) {
        return this.where(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryNestedWhereClauseBuilder andWhereLike(final String column, final String value) {
        return this.andWhere(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryNestedWhereClauseBuilder andWhereLike(final Column column, final String value) {
        return this.andWhere(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryNestedWhereClauseBuilder orWhereLike(final String column, final String value) {
        return this.orWhere(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryNestedWhereClauseBuilder orWhereLike(final Column column, final String value) {
        return this.orWhere(column, QwryConstants.Condition.LIKE, value);
    }

    public QwryNestedWhereClauseBuilder whereNull(final String column) {
        return this.where(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder whereNull(final Column column) {
        return this.where(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder andWhereNull(final String column) {
        return this.andWhere(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder andWhereNull(final Column column) {
        return this.andWhere(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder orWhereNull(final String column) {
        return this.orWhere(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder orWhereNull(final Column column) {
        return this.orWhere(column, QwryConstants.Condition.IS, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder whereNotNull(final String column) {
        return this.where(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder whereNotNull(final Column column) {
        return this.where(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder andWhereNotNull(final String column) {
        return this.andWhere(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder andWhereNotNull(final Column column) {
        return this.andWhere(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder orWhereNotNull(final String column) {
        return this.orWhere(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public QwryNestedWhereClauseBuilder orWhereNotNull(final Column column) {
        return this.orWhere(column, QwryConstants.Condition.IS_NOT, QwryConstants.Value.NULL);
    }

    public List<WhereClause> getWhereClauses() {
        return this._wheres;
    }
}
