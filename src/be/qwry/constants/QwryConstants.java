package be.qwry.constants;

/**
 * Created by Mathias Keustermans
 */
public final class QwryConstants {

    private QwryConstants()
    {
        //empty to avoid instantiating this constant class
    }

    public static class WhereType {
        public static final String AND = "AND".intern();
        public static final String OR = "OR".intern();
    }

    public static class Condition {
        public static final String IS = "IS".intern();
        public static final String IS_NOT = "IS NOT".intern();

        public static final String LIKE = "LIKE".intern();
    }

    public static class Value {
        public static final String NULL = "NULL".intern();
    }
}
