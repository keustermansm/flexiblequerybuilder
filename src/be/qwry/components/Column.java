package be.qwry.components;

/**
 * @author Mathias Keustermans
 */
public class Column {

    private String prefix;
    private String columnName;
    // private String alias;

    public Column(final String columnName) {
        this.columnName = columnName;
    }

    public Column(final String columnTableAlias, final String columnName) {
        this.prefix = columnTableAlias;
        this.columnName = columnName;
    }

    /*
    public Column as(final String alias) {
        this.alias = alias;
        return this;
    }
    */

    public String getPrefix() {
        return this.prefix;
    }

    public String getColumnName() {
        return columnName;
    }

}
