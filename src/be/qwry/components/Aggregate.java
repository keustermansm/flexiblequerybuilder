package be.qwry.components;

public class Aggregate {

    private String function;
    private String column;

    public Aggregate(String function, String column) {
        this.function = function;
        this.column = column;
    }

    public String getFunction() {
        return function;
    }

    public String getColumn() {
        return column;
    }
}
