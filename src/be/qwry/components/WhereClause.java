package be.qwry.components;

import java.util.List;

public class WhereClause {

    private Column column;
    private String operator;
    private Object value;
    private String type;

    private List<WhereClause> nestedClauses;

    public WhereClause(final Column column, final String operator, final Object value, final String type) {
        this.column = column;
        this.operator = operator;
        this.value = value;
        this.type = type;
    }

    public WhereClause(final List<WhereClause> nestedClauses, final String type) {
        this.nestedClauses = nestedClauses;
        this.type = type;
    }

    public Column getColumn() {
        return column;
    }

    public String getOperator() {
        return operator;
    }

    public Object getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    public List<WhereClause> getNestedClauses() {
        return nestedClauses;
    }
}
