package be.qwry;

/**
 * Static initializers for the QWRYBuilder
 *
 * @author Mathias Keustermans
 * @version 0.1
 */
public class Qwry {

    /**
     * Static initializer for the query builder
     *
     * @param type Item type used for the query
     * @return Instance of the QWRY builder.
     * @since 0.1
     */
    public static QwryBuilder type(final String type) {
        return new QwryBuilder(type);
    }

    /**
     * Static initializer for the query builder
     *
     * @param type Item type used for the query
     * @param alias Alias for the table
     * @return Instance of the QWRY builder.
     * @since 0.1
     */
    public static QwryBuilder type(final String type, final String alias) {
        return new QwryBuilder(type, alias);
    }
}





/*

"SELECT {taxRow:" + TaxRowModel.PK + "} " +
            "FROM {" +
                          DMLightsTaxRowModel._TYPECODE + " AS taxRow " +
                "JOIN " + TaxCountryModel._TYPECODE     + " AS taxShippingCountry ON {taxShippingCountry:" + TaxCountryModel.PK + "}   = {taxRow:" + DMLightsTaxRowModel.SHIPPINGTAXCOUNTRY + "} " +
                "JOIN " + CountryModel._TYPECODE        + " AS shippingCountry    ON {shippingCountry:" + CountryModel.TAXCOUNTRY + "} = {taxShippingCountry:" + TaxCountryModel.PK + "} " +
                "JOIN " + TaxCountryModel._TYPECODE     + " AS taxBillingCountry  ON {taxBillingCountry:" + TaxCountryModel.PK + "}    = {taxRow:" + DMLightsTaxRowModel.BILLINGTAXCOUNTRY + "} " +
                "JOIN " + CountryModel._TYPECODE        + " AS billingCountry     ON {billingCountry:" + CountryModel.TAXCOUNTRY + "}  = {taxBillingCountry:" + TaxCountryModel.PK + "} " +
            "}" +
            "WHERE " +
                    "{shippingCountry:" + CountryModel.PK + "} = ?shippingCountryPk    " +
                "AND {billingCountry:"  + CountryModel.PK + "} = ?billingCountryPk     " +
                "AND {taxRow:"          + TaxRowModel.UG  + "} = ?userTaxGroupPk       " +
                "AND {taxRow:"          + TaxRowModel.PG  + "} = ?productTaxGroupPk    " ;



 */
