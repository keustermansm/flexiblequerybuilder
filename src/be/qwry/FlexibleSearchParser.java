package be.qwry;

import be.qwry.components.Aggregate;
import be.qwry.components.Column;
import be.qwry.components.WhereClause;
import be.qwry.grammar.GrammerParser;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Grammer parser that translates the query into a FlexibleSearch Query
 *
 * @author Mathias Keustermans
 * @version 0.1
 */
public class FlexibleSearchParser implements GrammerParser {

    private static final String EMPTY_STRING = "";
    private static final String EXCLUDE_SUBTYPE_MARKER = "!";
    private static final String SPACE = " ";

    private static final String LEFT_CURLY_BRACE = "{";
    private static final String RIGHT_CURLY_BRACE = "}";

    private static final String LEFT_ROUND_BRACKET = "(";
    private static final String RIGHT_ROUND_BRACKET = ")";


    @Override
    public FlexibleSearchQuery compile(final QwryBuilder builder) {

        List<String> queryComponents = new LinkedList<>();
        queryComponents.add(compileAggregate(builder));
        queryComponents.add(compileColumns(builder));
        queryComponents.add(compileFrom(builder));
//        queryComponents.add(compileJoins(builder));
        queryComponents.add(compileWheres(builder));
//        queryComponents.add(compileGroups(builder));
//        queryComponents.add(compileHavings(builder));
//        queryComponents.add(compileOrders(builder));
//        queryComponents.add(compileLimit(builder));
//        queryComponents.add(compileOffset(builder));
//        queryComponents.add(compileUnions(builder));

        final FlexibleSearchQuery query = new FlexibleSearchQuery(this.concatenate(queryComponents));
        this.addQueryParameters(query, builder);
        return query;
    }

    private void addQueryParameters(final FlexibleSearchQuery query, final QwryBuilder builder) {

        for(final WhereClause where : builder._wheres) {

            if(where == null) {
                continue;
            }

            if(where.getNestedClauses() != null) {
                continue;
            }

            query.addQueryParameter(columnToString(where.getColumn()), where.getValue());
        }
    }

    private String columnToString(final Column column) {

        if(Strings.isNullOrEmpty(column.getPrefix())) {
            return column.getColumnName();
        }

        return column.getPrefix() + "." + column.getColumnName();
    }

    private String compileAggregate(final QwryBuilder builder) {

        Aggregate aggregate = builder._aggregate;

        if(aggregate == null) {
            return null;
        }

        String column = aggregate.getColumn();

        if(builder._isDistinct && !"*".equals(column)) {
            column = "DISTINCT " + column;
        }

        return "SELECT " + aggregate.getFunction() + "(" + column + ")";
    }

    private String compileColumns(final QwryBuilder builder) {

        if(builder._aggregate != null) {
            return null;
        }

        final StringBuilder selectBuilder = new StringBuilder("SELECT" + SPACE);

        if(builder._columns.isEmpty()) {
            selectBuilder.append("*");
            return selectBuilder.toString();
        }

        if(builder._isDistinct) {
            selectBuilder.append("DISTINCT" + SPACE);
        }

        final List<String> columns = new ArrayList<>();
        for (final Object column : builder._columns) {

            if(column instanceof String) {
                columns.add(surroundWithCurlyBraces((String) column));
                continue;
            }

            if (column instanceof Column) {
                final Column col = (Column) column;
                final String columnToAdd;

                if(Strings.isNullOrEmpty(col.getPrefix())) {
                    columnToAdd = col.getColumnName();
                } else {
                    columnToAdd = col.getPrefix() + "." + col.getColumnName();
                }

                columns.add(surroundWithCurlyBraces(columnToAdd));

                continue;
            }

            throw new IllegalArgumentException("Columns contains a non valid column definition!");
        }

        selectBuilder.append(concatenate(columns, ", "));

        return selectBuilder.toString().trim();
    }

    private String compileFrom(final QwryBuilder builder) {

        return "FROM " + surroundWithCurlyBraces(builder._tableName + checkForExcludeSuptypesMarker(builder) + checkForTableAlias(builder));
    }

    private String checkForExcludeSuptypesMarker(final QwryBuilder builder) {

        return (builder._includeSubTypes) ? EMPTY_STRING : EXCLUDE_SUBTYPE_MARKER;
    }

    private String checkForTableAlias(final QwryBuilder builder) {

        return Strings.isNullOrEmpty(builder._tableAlias) ? EMPTY_STRING : " AS " + builder._tableAlias;
    }

    private String compileWheres(final QwryBuilder builder) {
        if(builder._wheres.isEmpty()) {
            return null;
        }

        final List<String> whereBuilder = new ArrayList<>();

        whereBuilder.add("WHERE");

        for(final WhereClause where : builder._wheres) {

            if(where == null) {
                continue;
            }

            if(whereBuilder.size() > 1) {
                whereBuilder.add(where.getType());
            }

            if(where.getNestedClauses() != null && !where.getNestedClauses().isEmpty()) {
                whereBuilder.add(this.compileNestedWhereClauses(where.getNestedClauses()));
                continue;
            }

            whereBuilder.add(surroundWithCurlyBraces(columnToString(where.getColumn())));
            whereBuilder.add(where.getOperator());

            if("NULL".equals(where.getValue())) {
                whereBuilder.add("NULL");
            } else {
                // Create prepared statement with "?"-prefix
                whereBuilder.add("?" + where.getColumn().getColumnName());
            }
        }

        return concatenate(whereBuilder);
    }

    private String compileNestedWhereClauses(final List<WhereClause> whereClauses) {

        final List<String> nesteWhereBuilder = new ArrayList<>();

        for(final WhereClause where : whereClauses) {

            if(where == null) {
                continue;
            }

            if(nesteWhereBuilder.size() > 1) {
                nesteWhereBuilder.add(where.getType());
            }

            nesteWhereBuilder.add(surroundWithCurlyBraces(columnToString(where.getColumn())));
            nesteWhereBuilder.add(where.getOperator());
            // Create prepared statement with "?"-prefix
            nesteWhereBuilder.add("?" + where.getColumn().getColumnName());
        }

        return surroundWithRoundBrackets(concatenate(nesteWhereBuilder));
    }

//    private String compileOrders(final QWRYBuilder builder) {
//        if(Strings.isNullOrEmpty(builder._orderDirection)) {
//            return EMPTY_STRING;
//        }
//
//        return "ORDER BY " + builder._orderDirection + " "
//    }

    private String surroundWithCurlyBraces(final String input) {
        return LEFT_CURLY_BRACE + input + RIGHT_CURLY_BRACE;
    }

    private String surroundWithRoundBrackets(final String input) { return LEFT_ROUND_BRACKET + input + RIGHT_ROUND_BRACKET; }

    private String concatenate(List<String> queryComponents) {
        return concatenate(queryComponents, SPACE);
    }

    private String concatenate(List<String> queryComponents, String separator) {
        return Joiner.on(separator).skipNulls().join(queryComponents);
    }
}