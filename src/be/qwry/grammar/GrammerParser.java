package be.qwry.grammar;

import be.qwry.QwryBuilder;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

public interface GrammerParser {

    FlexibleSearchQuery compile(QwryBuilder builder);
}
