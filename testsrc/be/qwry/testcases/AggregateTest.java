package be.qwry.testcases;

import be.qwry.Qwry;
import be.qwry.models.TestProductModel;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Mathias Keustermans
 */
public class AggregateTest {

    @Test
    public void test_aggregate_count() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).count().getQuery();
        assertThat(buildQuery).isEqualTo("SELECT COUNT(*) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().count().getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT COUNT(*) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).count("pk").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT COUNT(pk) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery1 = Qwry.type(TestProductModel._TYPECODE).distinct().count("pk").getQuery();
        assertThat(buildQuery1).isEqualTo("SELECT COUNT(DISTINCT pk) FROM {" + TestProductModel._TYPECODE + "!}");
    }

    @Test
    public void test_aggregate_count_where_combination() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).where(TestProductModel.CODE, "00001").count().getQuery();
        assertThat(buildQuery).isEqualTo("SELECT COUNT(*) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").count().getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT COUNT(*) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).where(TestProductModel.CODE, "00001").count("pk").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT COUNT(pk) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery1 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").count("pk").getQuery();
        assertThat(buildQuery1).isEqualTo("SELECT COUNT(DISTINCT pk) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");
    }

    @Test
    public void test_aggregate_max() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).max("column1").getQuery();
        assertThat(buildQuery).isEqualTo("SELECT MAX(column1) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().max("*").getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT MAX(*) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).distinct().max("column1").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT MAX(DISTINCT column1) FROM {" + TestProductModel._TYPECODE + "!}");
    }

    @Test
    public void test_aggregate_max_where_combination() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).where(TestProductModel.CODE, "00001").max("column1").getQuery();
        assertThat(buildQuery).isEqualTo("SELECT MAX(column1) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").max("*").getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT MAX(*) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").max("column1").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT MAX(DISTINCT column1) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");
    }

    @Test
    public void test_aggregate_min() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).min("column1").getQuery();
        assertThat(buildQuery).isEqualTo("SELECT MIN(column1) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().min("*").getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT MIN(*) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).distinct().min("column1").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT MIN(DISTINCT column1) FROM {" + TestProductModel._TYPECODE + "!}");
    }

    @Test
    public void test_aggregate_min_where_combination() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).where(TestProductModel.CODE, "00001").min("column1").getQuery();
        assertThat(buildQuery).isEqualTo("SELECT MIN(column1) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").min("*").getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT MIN(*) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").min("column1").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT MIN(DISTINCT column1) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");
    }

    @Test
    public void test_aggregate_avg() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).avg("column1").getQuery();
        assertThat(buildQuery).isEqualTo("SELECT AVG(column1) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().avg("*").getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT AVG(*) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).distinct().avg("column1").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT AVG(DISTINCT column1) FROM {" + TestProductModel._TYPECODE + "!}");
    }

    @Test
    public void test_aggregate_avg_where_combination() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).where(TestProductModel.CODE, "00001").avg("column1").getQuery();
        assertThat(buildQuery).isEqualTo("SELECT AVG(column1) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").avg("*").getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT AVG(*) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").avg("column1").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT AVG(DISTINCT column1) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");
    }

    @Test
    public void test_aggregate_sum() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).sum("column1").getQuery();
        assertThat(buildQuery).isEqualTo("SELECT SUM(column1) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().sum("*").getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT SUM(*) FROM {" + TestProductModel._TYPECODE + "!}");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).distinct().sum("column1").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT SUM(DISTINCT column1) FROM {" + TestProductModel._TYPECODE + "!}");
    }

    @Test
    public void test_aggregate_sum_where_combination() {

        final String buildQuery = Qwry.type(TestProductModel._TYPECODE).where(TestProductModel.CODE, "00001").sum("column1").getQuery();
        assertThat(buildQuery).isEqualTo("SELECT SUM(column1) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery2 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").sum("*").getQuery();
        assertThat(buildQuery2).isEqualTo("SELECT SUM(*) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");

        final String buildQuery3 = Qwry.type(TestProductModel._TYPECODE).distinct().where(TestProductModel.CODE, "00001").sum("column1").getQuery();
        assertThat(buildQuery3).isEqualTo("SELECT SUM(DISTINCT column1) FROM {" + TestProductModel._TYPECODE + "!} WHERE {code} = ?code");
    }
}
