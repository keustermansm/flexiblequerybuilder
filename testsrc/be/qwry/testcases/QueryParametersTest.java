package be.qwry.testcases;

import be.qwry.Qwry;
import be.qwry.models.TestProductModel;
import com.google.common.collect.ImmutableMap;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Mathias Keustermans
 */
public class QueryParametersTest {

    @Test
    public void test_where_single_clause() {

        final FlexibleSearchQuery query = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", "12345").build();

        assertThat(query.getQuery()).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku");

        assertThat(query.getQueryParameters()).hasSize(1);

        assertThat(query.getQueryParameters()).isEqualTo(ImmutableMap.of("sku", "12345"));
    }

    @Test
    public void test_where_multiple_and_clauses() {

        final FlexibleSearchQuery query = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", "12345")
                .andWhere("price", ">", 12345)
                .build();

        assertThat(query.getQuery()).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku AND {price} > ?price");

        assertThat(query.getQueryParameters()).hasSize(2);

        assertThat(query.getQueryParameters()).isEqualTo(
                ImmutableMap.of(
                        "sku", "12345",
                        "price", 12345
                )
        );
    }

    @Test
    public void test_where_mixed_and_or_clauses() {

        final FlexibleSearchQuery query = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", "12345")
                .andWhere("price", ">", 12345)
                .orWhere("stock", 100)
                .build();

        assertThat(query.getQuery()).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku AND {price} > ?price OR {stock} = ?stock");

        assertThat(query.getQueryParameters()).hasSize(3);

        assertThat(query.getQueryParameters()).isEqualTo(
                ImmutableMap.of(
                        "price", 12345,
                        "sku", "12345",
                        "stock", 100
                )
        );
    }
}
