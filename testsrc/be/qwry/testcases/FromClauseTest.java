package be.qwry.testcases;

import be.qwry.Qwry;
import be.qwry.models.TestProductModel;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by mathias on 05/04/16.
 */
public class FromClauseTest {

    @Test
    public void test_from_without_subtypes() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE).build().getQuery();
        assertThat(queryString).isEqualTo("SELECT * FROM {" + TestProductModel._TYPECODE + "!}");
    }

    @Test
    public void test_from_include_subtypes() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE).includeSubtypes().build().getQuery();
        assertThat(queryString).isEqualTo("SELECT * FROM {" + TestProductModel._TYPECODE + "}");
    }

    @Test
    public void test_from_table_alias() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "productAlias").build().getQuery();
        assertThat(queryString).isEqualTo("SELECT * FROM {" + TestProductModel._TYPECODE + "! AS productAlias}");
    }

    @Test
    public void test_from_table_alias_include_subtypes() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "productAlias").includeSubtypes().build().getQuery();
        assertThat(queryString).isEqualTo("SELECT * FROM {" + TestProductModel._TYPECODE + " AS productAlias}");
    }
}
