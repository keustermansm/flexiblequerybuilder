package be.qwry.testcases;

import be.qwry.Qwry;
import be.qwry.models.TestProductModel;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static be.qwry.QwryBuilder.col;
import static org.fest.assertions.Assertions.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * @author Mathias Keustermans
 */
public class SelectClauseTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_select_clause_fallback() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE).build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {" + TestProductModel._TYPECODE + "!}");
    }

    @Test
    public void test_throw_exception_select_clause_is_null() {

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(equalTo("Select columns can not be null!"));

        Qwry.type(TestProductModel._TYPECODE).select(null).build().getQuery();
    }

    @Test
    public void test_select_multiple_columns() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE).select(TestProductModel.PK, TestProductModel.NAME, TestProductModel.CODE, TestProductModel.DESCRIPTION).build().getQuery();
        assertThat(queryString).isEqualTo("SELECT {" + TestProductModel.PK + "}, {" + TestProductModel.NAME + "}, {" + TestProductModel.CODE + "}, {" + TestProductModel.DESCRIPTION + "} FROM {" + TestProductModel._TYPECODE + "!}");
    }

    @Test
    public void test_select_multiple_prefixed_columns() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "product")
                .select(col(TestProductModel.PK), col("product", TestProductModel.NAME), col("product", TestProductModel.CODE), col("product", TestProductModel.DESCRIPTION))
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT {" + TestProductModel.PK + "}, {product." + TestProductModel.NAME + "}, {product." + TestProductModel.CODE + "}, {product." + TestProductModel.DESCRIPTION + "} FROM {" + TestProductModel._TYPECODE + "! AS product}");
    }
}
