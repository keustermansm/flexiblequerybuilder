package be.qwry.testcases;

/**
 * @author Mathias Keustermans
 */
public class JoinStatementTest {

    /*
    @Test
    public void test_join_basic() {

        Qwry.type("DMLightsTaxRowModel._TYPECODE","taxRow")
                .select()
                .join(TestProductModel._TYPECODE,"taxShippingCountry")
                    .on(
                        col("taxShippingCountry", TaxCountryModel.PK),
                        col("taxRow", TestProductModel.SHIPPINGTAXCOUNTRY)
                    )
                .join(CountryModel._TYPECODE,"shippingCountry").on(
                    col("shippingCountry", TaxCountryModel.PK),
                    col("taxShippingCountry", DMLightsTaxRowModel.SHIPPINGTAXCOUNTRY)
                )
                .whereNotIn()
                .build();


        Qwry.type("DMLightsTaxRowModel._TYPECODE","taxRow")
                .select()
                .join(TestProductModel._TYPECODE,"taxShippingCountry",
                        col("taxShippingCountry", TaxCountryModel.PK),
                        col("taxRow", TestProductModel.SHIPPINGTAXCOUNTRY)
                )


        Qwry.type("DMLightsTaxRowModel._TYPECODE","taxRow")
                .select()
                .join(TestProductModel._TYPECODE,"taxShippingCountry").on(
                col("taxShippingCountry", TaxCountryModel.PK),
                col("taxRow", TestProductModel.SHIPPINGTAXCOUNTRY)
        )
                .join(CountryModel._TYPECODE,"shippingCountry").on(
                col("shippingCountry", TaxCountryModel.PK),
                col("taxShippingCountry", DMLightsTaxRowModel.SHIPPINGTAXCOUNTRY)
        )
                .whereNotIn()
                .build();
    }
    */
}
