package be.qwry.testcases;

import be.qwry.Qwry;
import be.qwry.models.TestProductModel;
import org.junit.Test;

import static be.qwry.QwryBuilder.col;
import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Mathias Keustermans
 */
public class WhereClauseTest {

    @Test
    public void test_where_single_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                                        .where("sku", "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku");
    }

    @Test
    public void test_where_single_clause_with_condition() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", "<>", "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} <> ?sku");
    }

    @Test
    public void test_where_single_clause_with_column_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku");
    }

    @Test
    public void test_where_single_clause_with_condition_with_column_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "<>", "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} <> ?sku");
    }

    @Test
    public void test_where_multiple_where_clauses() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "12345").where("name", "Test Product").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku AND {name} = ?name");
    }

    @Test
    public void test_where_multiple_andWhere_clauses() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "12345").andWhere("name", "Test Product").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku AND {name} = ?name");
    }

    @Test
    public void test_where_multiple_andWhere_clauses_with_condition() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "12345")
                .andWhere("name", "<>", "Test Product")
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku AND {name} <> ?name");
    }

    @Test
    public void test_where_multiple_andWhere_clauses_with_prefixes() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "12345")
                .andWhere("name", "Test Product")
                .andWhere(col("p", "stock"), 5)
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku AND {name} = ?name AND {p.stock} = ?stock");
    }

    @Test
    public void test_where_multiple_andWhere_clauses_with_prefixes_and_condition() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "<>", "12345")
                .andWhere("name", "Test Product")
                .andWhere(col("p", "stock"), "<>", 5)
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} <> ?sku AND {name} = ?name AND {p.stock} <> ?stock");
    }

    @Test
    public void test_where_orWhere_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "12345")
                .orWhere("name", "Test Product")
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku OR {name} = ?name");
    }

    @Test
    public void test_where_orWhere_clause_with_condition() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "12345")
                .orWhere("name", "<>", "Test Product")
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku OR {name} <> ?name");
    }

    @Test
    public void test_where_multiple_orWhere_clauses() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "12345")
                .orWhere("name", "Test Product")
                .orWhere(col("p", "name"), "Test Product 2")
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku OR {name} = ?name OR {p.name} = ?name");
    }

    @Test
    public void test_where_multiple_orWhere_clauses_with_condition() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), "<>", "12345")
                .orWhere("name", "Test Product")
                .orWhere(col("p", "name"), "<>", "Test Product 2")
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} <> ?sku OR {name} = ?name OR {p.name} <> ?name");
    }

    @Test
    public void test_where_nested_andWhere_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), 12345)
                .andWhere(query ->
                        query.where("name", "test1")
                                .orWhere("stock", 100)
                )
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku AND ({name} = ?name OR {stock} = ?stock)");
    }

    @Test
    public void test_where_nested_andWhere_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), 12345)
                .andWhere(query ->
                        query.where("name", "test1")
                                .orWhere(col("p", "stock"), 100)
                )
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku AND ({name} = ?name OR {p.stock} = ?stock)");
    }

    @Test
    public void test_where_nested_andWhere_clause_with_prefix_and_condition() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), 12345)
                .andWhere(query ->
                        query.where(col("p", "name"), "=", "test1")
                                .orWhere(col("p", "stock"), ">", 100)
                )
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku AND ({p.name} = ?name OR {p.stock} > ?stock)");
    }

    @Test
    public void test_where_nested_orWhere_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), 12345)
                .orWhere(query ->
                        query.where("name", "test1")
                                .andWhere("stock", 100)
                )
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku OR ({name} = ?name AND {stock} = ?stock)");
    }

    @Test
    public void test_where_nested_orWhere_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), 12345)
                .orWhere(query ->
                        query.where("name", "test1")
                                .andWhere(col("p", "stock"), 100)
                )
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku OR ({name} = ?name AND {p.stock} = ?stock)");
    }

    @Test
    public void test_where_nested_orWhere_clause_with_prefix_and_condition() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE, "p")
                .where(col("p", "sku"), 12345)
                .orWhere(query ->
                        query.where(col("p", "name"), "!=", "test1")
                                .andWhere(col("p", "stock"), ">", 100)
                )
                .build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product! AS p} WHERE {p.sku} = ?sku OR ({p.name} != ?name AND {p.stock} > ?stock)");
    }

    @Test
    public void test_where_whereLike_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .whereLike("sku", "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} LIKE ?sku");
    }

    @Test
    public void test_where_whereLike_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .whereLike(col("p", "sku"), "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {p.sku} LIKE ?sku");
    }

    @Test
    public void test_where_andWhereLike_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("stock", 100)
                .andWhereLike("sku", "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {stock} = ?stock AND {sku} LIKE ?sku");
    }

    @Test
    public void test_where_andWhereLike_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("stock", 100)
                .andWhereLike(col("p", "sku"), "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {stock} = ?stock AND {p.sku} LIKE ?sku");
    }

    @Test
    public void test_where_orWhereLike_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("stock", 100)
                .orWhereLike("sku", "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {stock} = ?stock OR {sku} LIKE ?sku");
    }

    @Test
    public void test_where_orWhereLike_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("stock", 100)
                .orWhereLike(col("p", "sku"), "12345").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {stock} = ?stock OR {p.sku} LIKE ?sku");
    }

    @Test
    public void test_whereNull_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .whereNull("reference").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {reference} IS NULL");
    }

    @Test
    public void test_whereNull_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .whereNull(col("p", "reference")).build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {p.reference} IS NULL");
    }

    @Test
    public void test_andWhereNull_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", 12345)
                .andWhereNull("reference").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku AND {reference} IS NULL");
    }

    @Test
    public void test_where_andWhereNull_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", 12345)
                .andWhereNull(col("p", "reference")).build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku AND {p.reference} IS NULL");
    }

    @Test
    public void test_where_orWhereNull_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", 12345)
                .orWhereNull("reference").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku OR {reference} IS NULL");
    }

    @Test
    public void test_where_orWhereNull_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", 12345)
                .orWhereNull(col("p", "reference")).build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku OR {p.reference} IS NULL");
    }




    @Test
    public void test_where_whereNotNull_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .whereNotNull("reference").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {reference} IS NOT NULL");
    }

    @Test
    public void test_where_whereNotNull_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .whereNotNull(col("p", "reference")).build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {p.reference} IS NOT NULL");
    }

    @Test
    public void test_andWhereNotNull_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", 12345)
                .andWhereNotNull("reference").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku AND {reference} IS NOT NULL");
    }

    @Test
    public void test_andWhereNotNull_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", 12345)
                .andWhereNotNull(col("p", "reference")).build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku AND {p.reference} IS NOT NULL");
    }

    @Test
    public void test_orWhereNotNull_clause() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", 12345)
                .orWhereNotNull("reference").build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku OR {reference} IS NOT NULL");
    }

    @Test
    public void test_orWhereNotNull_clause_with_prefix() {

        final String queryString = Qwry.type(TestProductModel._TYPECODE)
                .where("sku", 12345)
                .orWhereNotNull(col("p", "reference")).build().getQuery();

        assertThat(queryString).isEqualTo("SELECT * FROM {Product!} WHERE {sku} = ?sku OR {p.reference} IS NOT NULL");
    }

    @Test
    public void test_multiple_nested_wheres_clauses() {
        // TODO make test
    }
}
