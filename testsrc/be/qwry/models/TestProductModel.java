package be.qwry.models;

/**
 * Created by mathias on 05/04/16.
 */
public class TestProductModel {

    public final static String _TYPECODE = "Product";

    public final static String PK = "pk";
    public final static String NAME = "name";
    public final static String CODE = "code";
    public static final String DESCRIPTION = "description";
}
