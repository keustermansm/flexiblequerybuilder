/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Apr 3, 2016 10:48:14 AM                     ---
 * ----------------------------------------------------------------
 */
package be.qwry.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFlexiblequerybuilderConstants
{
	public static final String EXTENSIONNAME = "flexiblequerybuilder";
	
	protected GeneratedFlexiblequerybuilderConstants()
	{
		// private constructor
	}
	
	
}
